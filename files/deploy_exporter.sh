#!/usr/bin/env bash

  set -o errexit   # abort on nonzero exitstatus
  set -o nounset   # abort on unbound variable
  set -o pipefail  # don't hide errors within pipes


docker_get_container_ip(){
   local container_prefix=$1
   export docker_container_name=$(docker ps --filter name=${container_prefix} --filter status=running -q)
   export docker_container_id=$(docker inspect ${docker_container_name} | jq .[].Id)
   printf "docker_container_id %s$docker_container_id\n" 
   export docker_mysql_container_ip=$(docker network inspect docker_gwbridge \
   | jq  ".[].Containers."$docker_container_id".IPv4Address" \
   | sed 's~[^\.[:alnum:]/]\+~~g' | cut -d'/' -f1)
   printf "docker_mysql_container_ip %s$docker_mysql_container_ip\n"
}
